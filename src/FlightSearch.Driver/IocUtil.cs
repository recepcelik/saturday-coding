﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using FlightSearch.Driver.Classes;

namespace FlightSearch.Driver
{
    public class IocUtil
    {
        private static IWindsorContainer _container;
        private static IWindsorContainer Container
        {
            get { return _container ?? (_container = SearchContainer()); }
        }
        private static IWindsorContainer SearchContainer()
        {
            return new WindsorContainer().Register(
                Component.For<IFlight>().ImplementedBy<IataSearch>(),
                Component.For<SearchClient>().ImplementedBy<SearchClient>());
        }
        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }
    }
}
